﻿using Google.Cloud.Firestore;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Web.MonicaMartinez.Service;
using Web.MonicaMartinez.Models.ViewModels;

namespace Web.MonicaMartinez.Controllers
{
    public class PatientsController : Controller
    {
        private readonly PatientService _service;

        public PatientsController()
        {
            _service = new PatientService();
        }

        // GET: Patients
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            #region select list

            var siafar = new SelectListGroup { Name = "SIAFAR" };
            var compania = new SelectListGroup { Name = "Compañia" };
            var misValidaciones = new SelectListGroup { Name = "Mis Validaciones" };
            var parqueSalud = new SelectListGroup { Name = "Parque Salud" };
            var colfacor = new SelectListGroup { Name = "Colfacor" };
            var imed = new SelectListGroup { Name = "IMED" };
            var mySelect = new List<SelectListItem>
                {
                    new SelectListItem{ Value = "Pami", Text = "Pami", Group = siafar},
                    new SelectListItem{ Value = "Iosfa", Text = "Iosfa", Group = siafar},
                    new SelectListItem{ Value = "MPN", Text = "MPN", Group = siafar},

                    new SelectListItem{ Value = "Avalian", Text = "Avalian", Group = compania},
                    new SelectListItem{ Value = "Nobis", Text = "Nobis", Group = compania},
                    new SelectListItem{ Value = "Perkins", Text = "Perkins", Group = compania},
                    new SelectListItem{ Value = "Osdepym", Text = "Osdepym", Group = compania},
                    new SelectListItem{ Value = "Ospe", Text = "Ospe", Group = compania},
                    new SelectListItem{ Value = "Ospim Ensalud", Text = "Ospim Ensalud", Group = compania},
                    new SelectListItem{ Value = "Ostel", Text = "Ostel", Group = compania},
                    new SelectListItem{ Value = "Prevencion Salud", Text = "Prevencion Salud", Group = compania},
                    new SelectListItem{ Value = "Sancor", Text = "Sancor", Group = compania},
                    new SelectListItem{ Value = "Staff Médico", Text = "Staff Médico", Group = compania},

                    new SelectListItem{ Value = "Osdop", Text = "Osdop", Group = misValidaciones},
                    new SelectListItem{ Value = "Osfatlyf", Text = "Osfatlyf", Group = misValidaciones},
                    new SelectListItem{ Value = "Ositac", Text = "Ositac", Group = misValidaciones},
                    new SelectListItem{ Value = "Osmata", Text = "Osmata", Group = misValidaciones},
                    new SelectListItem{ Value = "Bono Pap", Text = "Bono Pap", Group = misValidaciones},
                    new SelectListItem{ Value = "Prosalud", Text = "Prosalud", Group = misValidaciones},
                    new SelectListItem{ Value = "Sipssa", Text = "Sipssa", Group = misValidaciones},
                    new SelectListItem{ Value = "Unimed", Text = "Unimed", Group = misValidaciones},

                    new SelectListItem{ Value = "Parque Salud", Text = "Parque Salud", Group = parqueSalud},

                    new SelectListItem{ Value = "Recetario Solidario", Text = "Recetario Solidario", Group = colfacor},
                    new SelectListItem{ Value = "Jerarquicos Salud", Text = "Jerarquicos Salud", Group = colfacor},
                    new SelectListItem{ Value = "Swiss Medical ART", Text = "Swiss Medical ART", Group = colfacor},
                    new SelectListItem{ Value = "Experta ART", Text = "Experta ART", Group = colfacor},

                    new SelectListItem{ Value = "Apross", Text = "Apross", Group = imed},
                    new SelectListItem{ Value = "CPCE", Text = "CPCE", Group = imed},
                    new SelectListItem{ Value = "CPCE", Text = "Galeno", Group = imed},
                    new SelectListItem{ Value = "Poder Judicial", Text = "Poder Judicial", Group = imed},
                    new SelectListItem{ Value = "Omint", Text = "Omint", Group = imed},
                    new SelectListItem{ Value = "Osim", Text = "Osim", Group = imed},
                    new SelectListItem{ Value = "Ossacra", Text = "Ossacra", Group = imed},
                    new SelectListItem{ Value = "Swiss Medical", Text = "Swiss Medical", Group = imed},
                };

            #endregion

            try
            {
                var patients = _service.GetPatients().Result;
                ViewBag.selectList = mySelect;
                return View(patients);
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Patients/Details/5
        [HttpGet]
        [Authorize]
        public ActionResult Details(string id)
        {
            try
            {
                var patients = _service.GetPatientById(id).Result;
                return Json(patients, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return View("Error");
            }
        }

        // POST: Patients/Create
        [HttpPost]
        [Authorize]
        public ActionResult Create(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var model = new PatientViewModel()
                {
                    Nombre = collection["Nombre"],
                    Apellido = collection["Apellido"],
                    DNI = collection["DNI"],
                    Domicilio = collection["Domicilio"],
                    Telefono = collection["Telefono"],
                    Email = collection["Email"],
                    ObraSocial = collection["ObraSocial"],
                    Web = collection["Web"],
                    TipoNro = collection["TipoNro"],
                    Sucursal = collection["Sucursal"],
                    Fecha = collection["Fecha"],
                    MedHab = collection["MedHab"]
                };

                var result = _service.CreatePatient(model).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        // POST: Patients/Edit/5
        [HttpPost]
        [Authorize]
        public ActionResult Edit(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var model = new Dictionary<string, object>();
                model.Add("Id", collection["eId"]);
                model.Add("Nombre", collection["Nombre"]);
                model.Add("Apellido", collection["Apellido"]);
                model.Add("DNI", collection["DNI"]);
                model.Add("Domicilio", collection["Domicilio"]);
                model.Add("Telefono", collection["Telefono"]);
                model.Add("Email", collection["Email"]);
                model.Add("ObraSocial", collection["ObraSocial"]);
                model.Add("TipoNro", collection["TipoNro"]);
                model.Add("Web", collection["Web"]);
                model.Add("Sucursal", collection["Sucursal"]);
                model.Add("MedHab", collection["MedHab"]);

                var result = _service.EditPatient(model).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        // POST: Patients/Delete/5
        [HttpPost]
        [Authorize]
        public ActionResult Delete(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var result = _service.DeletePatient(collection["dId"]).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}
