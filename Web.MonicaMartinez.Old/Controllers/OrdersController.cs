﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web.MonicaMartinez.Service;
using Web.MonicaMartinez.Models.ViewModels;
using Web.MonicaMartinez.Models.DTOs;
using Microsoft.AspNet.Identity;

namespace Web.MonicaMartinez.Controllers
{
    public class OrdersController : Controller
    {
        private readonly OrderService _orderService;
        private readonly PatientService _patientService;

        public OrdersController()
        {
            _orderService = new OrderService();
            _patientService = new PatientService();

        }

        // GET: Orders
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            try
            {
                var ordersDto = _orderService.GetOrders().Result;
                var patientsDto = _patientService.GetPatients().Result;
                OrderViewModelList model = new OrderViewModelList();

                if (!ordersDto.Pedidos.Any())
                {
                    model.Pedidos = new List<OrderViewModel>();
                    model.Pacientes = patientsDto.Pacientes
                       .Select(x => new PatientViewModel()
                       {
                           Apellido = x.Apellido,
                           DNI = x.DNI,
                           Domicilio = x.Domicilio,
                           Email = x.Email,
                           Fecha = x.Fecha,
                           Id = x.Id,
                           MedHab = x.MedHab,
                           Nombre = x.Nombre,
                           ObraSocial = x.ObraSocial,
                           Sucursal = x.Sucursal,
                           Telefono = x.Telefono,
                           TipoNro = x.TipoNro,
                           Web = x.Web,
                           NombreTelefonoPaciente =
                                   $"{x.Nombre} {x.Apellido} ({ x.Telefono})"
                       }).ToList();
                }
                else
                {
                    model = new OrderViewModelList()
                    {
                        Pedidos = ordersDto.Pedidos
               .Select(x => new OrderViewModel()
               {
                   Detalle = x.Detalle,
                   Entregado = x.Entregado,
                   Fecha = DateTime.Parse(x.Fecha).ToString("dd/MM/yy"),
                   Id = x.Id,
                   OrdenNumero = x.OrdenNumero,
                   Preparar = x.Preparar,
                   Recibido = x.Recibido,
                   Sucursal = x.Sucursal,
                   IdPaciente = x.IdPaciente,
                   NombreTelefonoPaciente =
                       $"{patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Nombre} {patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Apellido} ({patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Telefono})",
                   ObraSocialPaciente = patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.ObraSocial,
               }).OrderByDescending(x => x.Fecha).ToList(),
                        Pacientes = patientsDto.Pacientes
                       .Select(x => new PatientViewModel()
                       {
                           Apellido = x.Apellido,
                           DNI = x.DNI,
                           Domicilio = x.Domicilio,
                           Email = x.Email,
                           Fecha = x.Fecha,
                           Id = x.Id,
                           MedHab = x.MedHab,
                           Nombre = x.Nombre,
                           ObraSocial = x.ObraSocial,
                           Sucursal = x.Sucursal,
                           Telefono = x.Telefono,
                           TipoNro = x.TipoNro,
                           Web = x.Web,
                           NombreTelefonoPaciente =
                                   $"{x.Nombre} {x.Apellido} ({ x.Telefono})"
                       }).ToList()
                    };
                }
                return View(model);
            }
            catch
            {
                return View("Error");
            }
        }

        // POST: Orders/Create
        [HttpPost]
        [Authorize]
        public ActionResult Create(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var model = new OrderDto
                {
                    OrdenNumero = collection["OrdenNumero"],
                    IdPaciente = collection["Paciente"],
                    Sucursal = collection["Sucursal"],
                    Fecha = collection["Fecha"],

                    Recibido = (!string.IsNullOrEmpty(collection["Recibido"]) &&
                        collection["Recibido"] == "recibido") ? true : false,

                    Preparar = (!string.IsNullOrEmpty(collection["Recibido"]) &&
                        collection["Recibido"] == "preparar") ? true : false,

                    Entregado = (!string.IsNullOrEmpty(collection["Entregado"]) &&
                        collection["Entregado"] == "entregado") ? true : false,

                    Detalle = collection["Detalle"],
                    CreadoPor = User.Identity.GetUserName()
                };

                var result = _orderService.CreateOrder(model).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        // POST: Patients/Edit/5
        [HttpPost]
        [Authorize]
        public ActionResult Edit(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var model = new Dictionary<string, object>
                {
                    { "Id", collection["eId"] },
                    { "OrdenNumero", collection["OrdenNumero"] },
                    { "Telefono", collection["Telefono"] },
                    { "Sucursal", collection["Sucursal"] },
                    {
                        "Recibido",
                        (!string.IsNullOrEmpty(collection["Recibido"]) &&
                            collection["Recibido"] == "recibido") ? true : false
                    },
                    {
                        "Preparar",
                        (!string.IsNullOrEmpty(collection["Recibido"]) &&
                            collection["Recibido"] == "preparar") ? true : false
                    },
                    {
                        "Entregado",
                        (!string.IsNullOrEmpty(collection["Entregado"]) &&
                            collection["Entregado"] == "entregado") ? true : false
                    },
                    { "Detalle", collection["Detalle"] },
                    {"CreadoPor", User.Identity.GetUserName() }

                };

                var result = _orderService.EditOrder(model).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult Update(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var model = new Dictionary<string, object>
                {
                    { "Id", collection["IdPedido"] },
                    { "Entregado", true },
                    //{ "fecha", DateTime.Today}
                    {"ActualizadoPor", User.Identity.GetUserName() }
                };

                var result = _orderService.EditOrder(model).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }


        // POST: Patients/Delete/5
        [HttpPost]
        [Authorize]
        public ActionResult Delete(FormCollection collection)
        {
            if (!ModelState.IsValid) return View("Error");
            try
            {
                var result = _orderService.DeleteOrder(collection["dId"]).Result;
                if (string.IsNullOrEmpty(result)) return View("Error");
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}