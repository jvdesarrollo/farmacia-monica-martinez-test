﻿using System.Collections.Generic;
using System.Web.Mvc;
using Web.MonicaMartinez.Service;
using Web.MonicaMartinez.Models.ViewModels;
using System.Linq;
using System;
using System.Globalization;

namespace Web.MonicaMartinez.Controllers
{
    public class HomeController : Controller
    {
        private readonly OrderService _orderService;
        private readonly PatientService _patientService;
        //private readonly CultureInfo _cultureInfo;

        public HomeController()
        {
            _orderService = new OrderService();
            _patientService = new PatientService();
            //_cultureInfo = new CultureInfo("en-US", true);
        }

        public ActionResult Index()
        {
            if (User?.Identity == null || !User.Identity.IsAuthenticated)
                return View(new DashboardViewModelList()
                {
                    PedidosRecibidos = new List<OrderViewModel>(),
                    PedidosPreparar = new List<OrderViewModel>(),
                    PedidosEntregados = new List<OrderViewModel>(),
                    PedidosHistoricos = new List<OrderViewModel>(),
                });

            var ordersDto = _orderService.GetOrders().Result;
            var patientsDto = _patientService.GetPatients().Result;

            DashboardViewModelList model;

            if (!ordersDto.Pedidos.Any())
            {
                model = (new DashboardViewModelList()
                {
                    PedidosRecibidos = new List<OrderViewModel>(),
                    PedidosPreparar = new List<OrderViewModel>(),
                    PedidosEntregados = new List<OrderViewModel>(),
                    PedidosHistoricos = new List<OrderViewModel>(),
                });
            }
            else
            {
                model = new DashboardViewModelList()
                {
                    PedidosRecibidos = ordersDto.Pedidos
                       .Where(x => !x.Entregado && x.Recibido && !x.Preparar &&
                           DateTime.Parse(x.Fecha).ToUniversalTime() >= DateTime.Today.AddDays(-7).ToUniversalTime())
                       .Select(x => new OrderViewModel()
                       {
                           Detalle = x.Detalle,
                           Entregado = x.Entregado,
                           Fecha = DateTime.Parse(x.Fecha).ToString("dd/MM/yy"),
                           Id = x.Id,
                           OrdenNumero = x.OrdenNumero,
                           Preparar = x.Preparar,
                           Recibido = x.Recibido,
                           Sucursal = x.Sucursal,
                           IdPaciente = x.IdPaciente,
                           NombreTelefonoPaciente =
                               $"{patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Nombre} {patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Apellido} ({patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Telefono})",
                           ObraSocialPaciente = patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.ObraSocial
                       }).ToList(),
                    PedidosPreparar = ordersDto.Pedidos
                       .Where(x => !x.Entregado && !x.Recibido && x.Preparar &&
                           DateTime.Parse(x.Fecha).ToUniversalTime() >= DateTime.Today.AddDays(-7).ToUniversalTime())
                       .Select(x => new OrderViewModel()
                       {
                           Detalle = x.Detalle,
                           Entregado = x.Entregado,
                           Fecha = DateTime.Parse(x.Fecha).ToString("dd/MM/yy"),
                           Id = x.Id,
                           OrdenNumero = x.OrdenNumero,
                           Preparar = x.Preparar,
                           Recibido = x.Recibido,
                           Sucursal = x.Sucursal,
                           IdPaciente = x.IdPaciente,
                           NombreTelefonoPaciente =
                               $"{patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Nombre} {patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Apellido} ({patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Telefono})",
                           ObraSocialPaciente = patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.ObraSocial
                       }).ToList(),
                    PedidosEntregados = ordersDto.Pedidos
                       .Where(x => x.Entregado &&
                           DateTime.Parse(x.Fecha).ToUniversalTime() >= DateTime.Today.AddDays(-7).ToUniversalTime())
                       .Select(x => new OrderViewModel()
                       {
                           Detalle = x.Detalle,
                           Entregado = x.Entregado,
                           Fecha = DateTime.Parse(x.Fecha).ToString("dd/MM/yy"),
                           Id = x.Id,
                           OrdenNumero = x.OrdenNumero,
                           Preparar = x.Preparar,
                           Recibido = x.Recibido,
                           Sucursal = x.Sucursal,
                           IdPaciente = x.IdPaciente,
                           NombreTelefonoPaciente =
                               $"{patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Nombre} {patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Apellido} ({patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Telefono})",
                           ObraSocialPaciente = patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.ObraSocial
                       }).ToList(),
                    PedidosHistoricos = ordersDto.Pedidos
                       .Where(x => DateTime.Parse(x.Fecha).ToUniversalTime() < DateTime.Today.AddDays(-7).ToUniversalTime())
                       .Select(x => new OrderViewModel()
                       {
                           Detalle = x.Detalle,
                           Entregado = x.Entregado,
                           Fecha = DateTime.Parse(x.Fecha).ToString("dd/MM/yy"),
                           Id = x.Id,
                           OrdenNumero = x.OrdenNumero,
                           Preparar = x.Preparar,
                           Recibido = x.Recibido,
                           Sucursal = x.Sucursal,
                           IdPaciente = x.IdPaciente,
                           NombreTelefonoPaciente =
                               $"{patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Nombre} {patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Apellido} ({patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.Telefono})",
                           ObraSocialPaciente = patientsDto.Pacientes.FirstOrDefault(y => y.Id == x.IdPaciente)?.ObraSocial
                       }).ToList(),
                };
            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}