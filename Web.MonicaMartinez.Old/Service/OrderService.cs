﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using Web.MonicaMartinez.Models.DTOs;
using Web.MonicaMartinez.Models.ViewModels;

namespace Web.MonicaMartinez.Service
{
    public interface IOrderService
    {
        Task<OrderDtoList> GetOrders();

        Task<OrderViewModel> GetOrderById(string id);

        Task<string> CreateOrder(OrderDto model);

        Task<string> EditOrder(Dictionary<string, object> model);

        Task<string> DeleteOrder(string Id);

    }

    public class OrderService : IOrderService
    {
        private readonly FirestoreDb _db;
        private readonly bool _getTimers = bool.Parse(WebConfigurationManager.AppSettings["getTimers"]);

        public OrderService()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + @"cloudmonicamartinez-firebase.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);

            _db = FirestoreDb.Create("cloudmonicamartinez-a410e");
        }

        public async Task<OrderDtoList> GetOrders()
        {
            Query qRef = _db.Collection("orders");

            var sw = new Stopwatch();
            sw.Start();

            var qs = qRef.GetSnapshotAsync().Result;

            sw.Stop();

            if (_getTimers)
            {
                var cr = _db.Collection("diagnostics");
                var pepe = new
                {
                    Name = "GetOrders",
                    Value = sw.Elapsed.TotalMilliseconds,
                    DT = DateTime.Now.ToString()
                };

                _ = cr.AddAsync(pepe);
            }

            if (!qs.Any()) return new OrderDtoList() { Pedidos = new List<OrderDto>() };

            var resultList = new OrderDtoList() { Pedidos = new List<OrderDto>() };
            foreach (var ds in qs)
            {
                var orden = ds.ConvertTo<OrderDto>();
                orden.Id = ds.Id;
                resultList.Pedidos.Add(orden);
            }

            return await Task.FromResult(resultList);
        }

        public async Task<OrderViewModel> GetOrderById(string id)
        {
            var dRef = _db.Collection("orders").Document(id);
            var ds = dRef.GetSnapshotAsync().Result;
            OrderViewModel model = null;

            if (ds.Exists)
            {

                model = ds.ConvertTo<OrderViewModel>();
            }

            return await Task.FromResult(model);
        }

        public async Task<string> CreateOrder(OrderDto model)
        {
            var cr = _db.Collection("orders");
            var result = cr.AddAsync(model).Result;

            return await Task.FromResult(result.Id);
        }

        public async Task<string> EditOrder(Dictionary<string, object> model)
        {
            var cr = _db.Collection("orders");
            var result = cr.Document(model["Id"].ToString())
                .UpdateAsync(model).Result;

            return await Task.FromResult(result.ToString());
        }

        public async Task<string> DeleteOrder(string Id)
        {
            var cr = _db.Collection("orders");
            var result = cr.Document(Id).DeleteAsync().Result;

            return await Task.FromResult(result.ToString());
        }

    }
}