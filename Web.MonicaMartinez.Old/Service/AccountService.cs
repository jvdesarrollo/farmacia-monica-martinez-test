﻿using Firebase.Auth;
using Google.Cloud.Firestore;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Web.MonicaMartinez.Service
{
    public class AccountService
    {
        private readonly FirestoreDb db;
        private readonly bool _getTimers = bool.Parse(WebConfigurationManager.AppSettings["getTimers"]);
        private readonly string _apiKey = WebConfigurationManager.AppSettings["apiKey"];

        public AccountService()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + @"cloudmonicamartinez-firebase.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);

            db = FirestoreDb.Create("cloudmonicamartinez-a410e");
        }

        public async Task<SignInStatus> Login(string email, string password)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(_apiKey));

            try
            {
                var sw = new Stopwatch();
                sw.Start();

                var res = auth.SignInWithEmailAndPasswordAsync(email, password).Result;
                if (res.User == null) throw new Exception("Error en el metodo SignInWithEmailAndPasswordAsync de firebase.");

                sw.Stop();

                if (_getTimers)
                {
                    var cr = db.Collection("diagnostics");
                    var pepe = new
                    {
                        Name = "Login",
                        Value = sw.Elapsed.TotalMilliseconds,
                        DT = DateTime.Now.ToString()
                    };

                    _ = cr.AddAsync(pepe);
                }

            }
            catch (Exception ex)
            {
                return Task.FromResult(SignInStatus.Failure).Result;
            }

            return Task.FromResult(SignInStatus.Success).Result;
        }
        public async Task<SignInStatus> Register(string email, string password)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(_apiKey));

            try
            {
                var sw = new Stopwatch();
                sw.Start();

                var res = auth.CreateUserWithEmailAndPasswordAsync(email, password).Result;
                if (res.User == null) throw new Exception("Error en el metodo CreateUserWithEmailAndPasswordAsync de firebase.");

                sw.Stop();

                if (_getTimers)
                {
                    var cr = db.Collection("diagnostics");
                    var pepe = new
                    {
                        Name = "Login",
                        Value = sw.Elapsed.TotalMilliseconds,
                        DT = DateTime.Now.ToString()
                    };

                    _ = cr.AddAsync(pepe);
                }

            }
            catch (Exception ex)
            {
                return Task.FromResult(SignInStatus.Failure).Result;
            }

            return Task.FromResult(SignInStatus.Success).Result;
        }

        public async Task<SignInStatus> ChangePassword(string email)
        {
            var auth = new FirebaseAuthProvider(new FirebaseConfig(_apiKey));

            try
            {
                var sw = new Stopwatch();
                sw.Start();

                auth.SendPasswordResetEmailAsync(email);
                //if (res. == null) throw new Exception("Error en el metodo CreateUserWithEmailAndPasswordAsync de firebase.");

                sw.Stop();

                if (_getTimers)
                {
                    var cr = db.Collection("diagnostics");
                    var pepe = new
                    {
                        Name = "Login",
                        Value = sw.Elapsed.TotalMilliseconds,
                        DT = DateTime.Now.ToString()
                    };

                    _ = cr.AddAsync(pepe);
                }

            }
            catch (Exception ex)
            {
                return Task.FromResult(SignInStatus.Failure).Result;
            }

            return Task.FromResult(SignInStatus.Success).Result;
        }
    }
}