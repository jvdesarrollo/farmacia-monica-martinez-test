﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Web.MonicaMartinez.Models.ViewModels;

namespace Web.MonicaMartinez.Service
{
    public interface IPatientService
    {
        Task<PatientViewModelList> GetPatients();
        Task<PatientViewModel> GetPatientById(string id);
        Task<string> CreatePatient(PatientViewModel model);
        Task<string> EditPatient(Dictionary<string, object> model);
        Task<string> DeletePatient(string Id);
    }

    public class PatientService : IPatientService
    {
        private readonly FirestoreDb db;

        public PatientService()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + @"cloudmonicamartinez-firebase.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);

            db = FirestoreDb.Create("cloudmonicamartinez-a410e");
        }

        public async Task<PatientViewModelList> GetPatients()
        {
            Query qRef = db.Collection("patients");
            var qs = qRef.GetSnapshotAsync().Result;

            if (!qs.Any()) return new PatientViewModelList() { Pacientes = new List<PatientViewModel>() };

            var resultList = new PatientViewModelList() { Pacientes = new List<PatientViewModel>() };
            foreach (var ds in qs)
            {
                var paciente = ds.ConvertTo<PatientViewModel>();
                paciente.Id = ds.Id;
                resultList.Pacientes.Add(paciente);
            }

            return await Task.FromResult(resultList);
        }

        public async Task<PatientViewModel> GetPatientById(string id)
        {
            var dRef = db.Collection("patients").Document(id);
            var ds = /*await dr.GetSnapshotAsync();*/ dRef.GetSnapshotAsync().Result;
            PatientViewModel model = null;

            if (ds.Exists)
            {
                #region old

                //model = new PatientViewModel();
                //var patientResult = ds.ToDictionary();
                //string patient = string.Empty;
                //foreach (var item in patientResult)
                //{
                //    //patient += string.Format("{0}: {1}", item.Key, item.Value);
                //    switch (item.Key)
                //    {
                //        case "Nombre":
                //            model.Nombre = item.Value.ToString();
                //            break;
                //        case "Apellido":
                //            model.Apellido = item.Value.ToString();
                //            break;
                //        case "DNI":
                //            model.DNI = item.Value.ToString();
                //            break;
                //        case "Domicilio":
                //            model.Domicilio = item.Value.ToString();
                //            break;
                //        case "Telefono":
                //            model.Telefono = item.Value.ToString();
                //            break;
                //        case "Email":
                //            model.Email = item.Value.ToString();
                //            break;
                //        case "ObraSocial":
                //            model.ObraSocial = item.Value.ToString();
                //            break;
                //        case "TipoNro":
                //            model.TipoNro = item.Value.ToString();
                //            break;
                //        case "MedHab":
                //            model.MedHab = item.Value.ToString();
                //            break;
                //        default:
                //            break;
                //    }
                //}
                //return patient;


                #endregion
                model = ds.ConvertTo<PatientViewModel>();
            }

            return await Task.FromResult(model);
        }

        public async Task<string> CreatePatient(PatientViewModel model)
        {
           var cr = db.Collection("patients");
           var result = cr.AddAsync(model).Result;

           return await Task.FromResult(result.Id);
        }

        public async Task<string> EditPatient(Dictionary<string, object> model)
        {
            var cr = db.Collection("patients");
            var result = cr.Document(model["Id"].ToString())
                .UpdateAsync(model).Result;

            return await Task.FromResult(result.ToString());
        }

        public async Task<string> DeletePatient(string Id)
        {
            var cr = db.Collection("patients");
            var result = cr.Document(Id).DeleteAsync().Result;

            return await Task.FromResult(result.ToString());
        }
    }
}
