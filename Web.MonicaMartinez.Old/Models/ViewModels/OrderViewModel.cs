﻿using System;
using System.Collections.Generic;

namespace Web.MonicaMartinez.Models.ViewModels
{
    public class OrderViewModel
    {
        public string Id { get; set; }
        public string Sucursal { get; set; }
        public string OrdenNumero { get; set; }
        public bool Recibido { get; set; }
        public bool Preparar { get; set; }
        public bool Entregado { get; set; }
        public string Detalle { get; set; }
        public string Fecha { get; set; }
        public string IdPaciente { get; set; }
        public string NombreTelefonoPaciente { get; set; }
        public string ObraSocialPaciente { get; set; }
    }

    public class OrderViewModelList
    {
        public List<OrderViewModel> Pedidos { get; set; }

        public List<PatientViewModel> Pacientes { get; set; }
    }
}