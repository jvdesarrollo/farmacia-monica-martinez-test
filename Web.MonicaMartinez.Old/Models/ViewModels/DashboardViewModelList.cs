﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.MonicaMartinez.Models.ViewModels
{
    public class DashboardViewModelList
    {
        public List<OrderViewModel> PedidosRecibidos { get; set; }
        public List<OrderViewModel> PedidosPreparar { get; set; }
        public List<OrderViewModel> PedidosEntregados { get; set; }
        public List<OrderViewModel> PedidosHistoricos { get; set; }
    }
}