﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.MonicaMartinez.Models.ViewModels
{
    [FirestoreData]
    public class PatientViewModel
    {
        public string Id { get; set; }

        [FirestoreProperty]
        public string Nombre { get; set; }

        [FirestoreProperty]
        public string Apellido { get; set; }

        [FirestoreProperty]
        public string DNI { get; set; }

        [FirestoreProperty]
        public string Domicilio { get; set; }

        [FirestoreProperty]
        public string Telefono { get; set; }

        [FirestoreProperty]
        public string Email { get; set; }

        [FirestoreProperty]
        public string ObraSocial { get; set; }

        [FirestoreProperty]
        public string Web { get; set; }

        [FirestoreProperty]
        public string TipoNro { get; set; }

        [FirestoreProperty]
        public string Sucursal { get; set; }

        [FirestoreProperty]
        public string Fecha { get; set; }

        [FirestoreProperty]
        public string MedHab { get; set; }
        public string NombreTelefonoPaciente { get; internal set; }
    }

    public class PatientViewModelList
    {
        public List<PatientViewModel> Pacientes { get; set; }
    }
}