﻿using Google.Cloud.Firestore;
using System.Collections.Generic;

namespace Web.MonicaMartinez.Models.DTOs
{
    [FirestoreData]
    public class OrderDto
    {
        public string Id { get; set; }

        [FirestoreProperty]

        public string Sucursal { get; set; }

        [FirestoreProperty]
        public string IdPaciente { get; set; }

        [FirestoreProperty]
        public string OrdenNumero { get; set; }

        [FirestoreProperty]
        public bool Recibido { get; set; }

        [FirestoreProperty]
        public bool Preparar { get; set; }

        [FirestoreProperty]
        public bool Entregado { get; set; }

        [FirestoreProperty]
        public string Detalle { get; set; }

        [FirestoreProperty]
        public string Fecha { get; set; }
        
        [FirestoreProperty]
        public string CreadoPor { get; set; }

        [FirestoreProperty]
        public string ActualizadoPor { get; set; }

    }

    public class OrderDtoList
    {
        public List<OrderDto> Pedidos { get; set; }
    }
}