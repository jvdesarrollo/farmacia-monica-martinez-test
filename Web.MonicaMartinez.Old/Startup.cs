﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Web.MonicaMartinez.Startup))]
namespace Web.MonicaMartinez
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
